'use strict'

//funciones anonimas
//Es una funcion que no tiene nombre
//se le asigna la funcion anonima a una variable
/*var pelicula = function (nombre) {
    return "La pelicula es: " + nombre
}*/




function sumame(numero1, numero2, sumaYmuestra, sumaPorDos) {
    var sumar = numero1 + numero2;
sumaYmuestra(sumar);
sumaPorDos(sumar);

    return sumar;
}

sumame(5, 7, function (dato) {
    console.log("La suma es : ", dato);
},//esta es la funcion flecha es lo mismo que poner function
     (dato)=> {
        console.log("La suma por dos es: ", (dato * 2));
    }

)
    ;
