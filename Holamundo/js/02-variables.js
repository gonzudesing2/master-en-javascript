//hace que se adecue a mejores standares
'use strict'

//VARIABLES se puede usar la palabra reservada var o let
//Una variable es un contenedor de informacion 


// asi son las variables en javascript
var pais = "España";
var contiente ="Europa";
var antiguedad = 2019;
var pais_y_continete = pais+' '+contiente;

// otra forma de declarar variables es usar  la palabra reservada let que  viene de typescript
//let define variables limitadas al bloque en que se esta usando(locales) y var declara variables globales
let prueba = "hola";


pais ="Mexico";

console.log(pais,contiente,antiguedad);
alert(pais_y_continete+" "+antiguedad);
alert(prueba);