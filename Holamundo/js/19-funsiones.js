'use strict'


//Funciones
//Una funsion es una agrupacion reutilizable de un conjunto de instrucciones



//Defino la funsion
//Los parametros ()  son lo que la funsion va resivir
// mostrar es un parametro opcional
function calculadora(numero1,numero2, mostrar=false) {
   // return "Hola soy la calculadora";
   if(mostrar=false){

       console.log("Suma: " + (numero1 + numero2));
       console.log("Resta: " + (numero1 - numero2));
       console.log("Multiplicacion: " + (numero1 * numero2));
       console.log("Division: " + (numero1 / numero2));
       console.log(mostrar);
       console.log("---------------------------------------------------");

   }else{
   document.write("Suma: " +(numero1+numero2)+"</br>");
    document.write("Resta: " + (numero1 - numero2) + "</br>");
    document.write("Multiplicacion: " + (numero1 * numero2) + "</br>"); 
    document.write("Division: " + (numero1 / numero2) + "</br>");
document.write(mostrar);
document.write("---------------------------------------------------");


   //return "hola soy la calculadora";
}
}

//Llamo o invoco la funsion cuantas veces necesito
// si llamo solo la funsion no me devuelve el texto del return
//En este caso le estamos pasando valores al parametro detiados ingresados  a la fuerza
calculadora(2,5,true);
//calculadora(10, 2);

/*
//Pero si lo guardo en una variable si me lo devuelve por que hago un console.log()
var resultado = calculadora();
console.log(resultado);

*/
//Se puede utilizar  en bucles tambien
/*
for(var i =1; i<=10; i++){
    console.log(i);
calculadora(i,8);
}
*/

