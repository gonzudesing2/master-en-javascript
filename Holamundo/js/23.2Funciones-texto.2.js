
'use strict'

//Transformacion de cadenas de textos

var numero = 444;
var texto1 = "Bienvenido al curso de Jvascript curso curso"
var texto2 = "Es muy buen curso curso curso";

//indexof busca la palabra en el objeto(variable) ojo primer coincidencia
var busqueda = texto1.indexOf("curso");

//lastIndexof busca la palabra en el objeto(variable) ojo ultima coincidencia
var busqueda = texto1.lastIndexOf("curso");


console.log(busqueda);

//search tambien permite buscar ES casi lo mismo que index off
var busqueda = texto1.search("curso");


console.log(busqueda);

//match tambien busca pero con match y devuelva una coleccion de resultados
//solo encuentra el primer resultado
var busqueda = texto1.match("curso");


console.log(busqueda);

//match tambien busca pero con match y devuelva una coleccion de resultados
//   /curso/gi hace busqueda global y trae todos los resultados
var busqueda = texto1.match(/curso/gi);
console.log(busqueda);

//subtr busca de un parametro en adelante el otro parametro del 14 5 despues
var busqueda = texto1.substr(14,5);
console.log(busqueda);

//charAT busca la letra en el numero dado
var busqueda = texto1.charAt(44);
console.log(busqueda);


//starsWith busca la palabra al inicio y devuelve un booleano
var busqueda = texto1.startsWith("curso");
console.log(busqueda);

//endsWith busca la palabra al final y devuelve un booleano
var busqueda = texto1.endsWithWith("curso");
console.log(busqueda);


//includes busca la palabra  y devuelve un booleano
var busqueda = texto1.includes("curso");
console.log(busqueda);


//replace busca la palabra  y la remplasa por el parametro de la derecha
var busqueda = texto1.replace("curso","remplazado");
console.log(busqueda);


//slice devuelve el texto apartir del carcter dado en el parametro o puede usar dos parametros para delimitar(14,25)
var busqueda = texto1.slice(14);
console.log(busqueda);

//splite mete el texto en un arrray si se quiere s ele asigna un sperador (" ").
var busqueda = texto1.splite(" ");
console.log(busqueda);


//trim recorta los pespacios por delante y por detras
//super util para recoger datos de formularios
var busqueda = texto1.trim();
console.log(busqueda);


//uso de plantillas





