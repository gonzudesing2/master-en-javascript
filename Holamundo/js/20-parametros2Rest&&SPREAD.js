'use strict'

//Parametros Rest y SPREAD
//... crea un array de parametros restantes se llama rest
function listadoFrutas(fruta1,fruta2, ...resto_de_frutas) {
    console.log("Fruta1: " + fruta1);
    console.log("Fruta2: " +  fruta2);
console.log(resto_de_frutas);
}

listadoFrutas("Naranja","Manzana","Sandia","Pera","Melon","Coco");

//En el spread se crea una variable array y con los tres puntos se carga el valor del array y se pasa como parametro
var frutas = ["Naranja", "Manzana","Prueba"];
listadoFrutas(...frutas, "Manzana", "Sandia", "Pera", "Melon", "Coco");