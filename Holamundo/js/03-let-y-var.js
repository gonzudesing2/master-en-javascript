'use strict'
//pruebas con let y var
//Prueba con var
var numero = 40;
console.log(numero);//valor 40

if (true) {
    var numero = 50;
    console.log(numero);//valor 50
}

console.log(numero);//valor 50

//Prueba con let


var texto = "loremmsksmmlnoahoiahohs"

console.log(texto);//valor

// LET es local y solo a nivel del bloque ya afuera no
if (true) {
    let texto = "aaaaaaaaaa"
    console.log(texto);
} 

console.log(texto);
