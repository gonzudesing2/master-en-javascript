'use strict'

//bucle while
//siempre usar un incrementador para que no haga infinito



var year = 2018;


while (year <= 2051) {
    //ejecuta esto

    console.log("Estamos en el año" +year);


    if (year == 2030) {
        break;
    }
    
    year++;
}




//bucle do while

var years = 30;
do {
    
    alert("el numero es "+years);
    years --;
} while (years > 25);