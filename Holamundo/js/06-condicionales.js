"use strict"


//Condicional if
//Si se cumple la condicion ejecute la instruccion sino(else) ejecuta la otra instruccion


var edad1 = 10;
var edad2 = 12;


// si pasa esto
if (edad1>edad2) {
    //ejecuta esto
console.log("Edad 1 es mayor que edad 2")

}else{

console.log("Edad 1 es inferior que edad 2");

}

/*Operadores condicionales

Mayor:>
Menor:<
Mayor o igual: >=
Menor o igual: <=
Igual: ==
Distinto: !=

*/


// Ejemplo 2
var edad = 17;
var nombre = "Jose Carlos" ;


if (edad > 18 ) {
    //es mayor

console.log(nombre+" tiene "+edad+" años es mayor de edad")

} else {
    console.log(nombre + " tiene " + edad + " años es menor de edad")
}



// Ejemplo 3 if anidado y elseif que lo que hace es en el else enves de arrojar de una ves la instruccion volver a validar
var edad = 74;
var nombre = "Jose Carlos";


if (edad > 18) {
    //es mayor

    console.log(nombre + " tiene " + edad + " años es mayor de edad")

if (edad == 33) {
    console.log("todavia eres milenial");
} else if(edad >= 70){
    console.log("eres ciudadano de oro");
}else{console.log("Ya no eres milenial");}

} else {
    console.log(nombre + " tiene " + edad + " años es menor de edad")
}



/*
//Operadores logicos

AND(Y): &&
OR(O): ||
NEGACION: !



*/



//Negacion


var year = 2018;

if (year !=2016) {
  
    console.log("El año no es 2016 realmente es " + year);
} 


//AND

if (year >= 2000 && year  <= 2020 && year != 2018) {
    console.log("Estamos en la era actual ");
}else{

    console.log("Estamos en la era postmoderna")
}


// or  si se usan los parentesis se puen meter mas operadores y validaciones a demas del and

if ( year == 2008 || (year >= 2018 && year == 2028)  ) {
    console.log("El año acaba en 8");
}else{

    console.log("Año no registrado");
}