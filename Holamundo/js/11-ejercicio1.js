'use strict'

/*
Programa que pida dos numeros y q nos diga cual es mayor y menor y si son iguales

nota el rompt recoge strings entonces es necesario el convertir el resultado a int
 para poder aplicar operaciones a el

 plus:si los numeros no son numeros o son menores o igual a cero nos vuelva apedir numeros.
*/

var numero1 = parseInt(prompt("Introduce el primer numero",0));
var numero2 = parseInt(prompt("Introduce el segundo numero", 0));


// asi validamos el plus del ejercicio creamos un while que haga todas las vlaidaciones con or ||
//y hasta que ninnguna se cumpla se seguira repitiendo hasta que el numero sea correcto

while (numero1 <= 0 || numero2<=0 || isNaN(numero1) || isNaN(numero2)) {
    numero1 = parseInt(prompt("Introduce el primer numero", 0));
    numero2 = parseInt(prompt("Introduce el segundo numero", 0));
}

if (numero1 == numero2) {
    alert("los numeros ingresados son iguales")
}else if (numero1>numero2) {
    alert("el numero mayor es: "+ numero1);
    alert("el numero menor es: "+numero2);
}else if (numero2>numero1) {
    alert("el numero mayor es: " + numero2);
    alert("el numero menor es: " + numero1);
}else{
    alert("introduce numeros validos");
}

/*
switch (numero1,numero2) {
    case numero1==numero2:
        alert("los numeros ingresados son iguales")
        break;



    case numero1 > numero2:

        alert("el numero mayor es: " + numero1);
        alert("el numero menor es: " + numero2);

    break;

    case numero2 > numero1:

        alert("el numero mayor es: " + numero2);
        alert("el numero menor es: " + numero1);

    default:
        alert("introduce numeros validos");
        break;
}*/