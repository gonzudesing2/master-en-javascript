'use stric'

/*
Crear calculadora
-Pida dos numeros por pantalla
-Si metemos un numero mal que nos lo vuelva a pedir
-En el cuerpo de la pagina, en una alerta y por la consola el resultado de sumar,
restar,multiplicar y dividir esas dos cifras

*/

var numero1 = parseInt(prompt("Introduce el primer numero", 0));
var numero2 = parseInt(prompt("Introduce el segundo numero", 0));



while (numero1<0 || numero2<0 || isNaN(numero1) || isNaN(numero2)) {

    //Nota cuando no se esta definiendo la variable por primera vez sino que se le esta asignando un valor
    //no se le pone la palabra reservada vaar 
     numero1 = parseInt(prompt("Introduce el primer numero", 0));
     numero2 = parseInt(prompt("Introduce el segundo numero", 0));

}
var resultado = "La suma es: "+(numero1+numero2)+"</br>"+
                "La resta es: " + (numero1 - numero2) + "</br>" +
                "La multiplicacion es: " + (numero1 * numero2) + "</br>" +
                 "La divicion es: " + (numero1 / numero2) + "</br>" ;

                 document.write(resultado);
                 
                 
//Nota:para hacer saltos de lineas tanto en consola como en alert se utiliza \n


var resultadoCMD = "La suma es: " + (numero1 + numero2) + " \n" +
    "La resta es: " + (numero1 - numero2) + " \n" +
    "La multiplicacion es: " + (numero1 * numero2) + " \n" +
    "La divicion es: " + (numero1 / numero2) + " \n";


document.write(resultado);
alert(resultadoCMD);
console.log(resultadoCMD);